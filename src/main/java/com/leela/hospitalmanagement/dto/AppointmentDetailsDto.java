package com.leela.hospitalmanagement.dto;

import java.sql.Date;

import javax.persistence.Column;

import lombok.Data;

@Data
public class AppointmentDetailsDto {

	private Integer appointmentId;
	
	private Integer patientId;
	
	private Integer doctorId;
	
	private Date date;
	
	private String time;
	
	private String isConfirmed;
	
	private String prescription;
}
