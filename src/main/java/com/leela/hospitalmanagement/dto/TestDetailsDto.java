package com.leela.hospitalmanagement.dto;

import java.sql.Date;

import javax.persistence.Column;

import lombok.Data;

@Data
public class TestDetailsDto {


	private Integer testId;
	
	private Integer patientId;
	
	private Integer appointmentId;
	
	private String name;
	
	private Date date;
	
	private Integer doctorId;
	
	private String remarks;
	
	
}
