package com.leela.hospitalmanagement.dto;

import lombok.Data;

@Data
public class DoctorScheduleDto {


	private Integer scheduleId;
	
	private Integer doctorId;
	
	private Integer totalAvailableSlots;
	
	private Integer presentAvaialbeSlots;
	
	private String scheduleTimings;
	
}
