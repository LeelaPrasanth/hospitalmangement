package com.leela.hospitalmanagement.dto;

import lombok.Data;

@Data
public class ResponseStructure<T> {

	private T data;
	private String message;
	private int statusCode;
	
	public ResponseStructure() {
		
	}
	
	public ResponseStructure(T data, String message, int statusCode) {
		super();
		this.data = data;
		this.message = message;
		this.statusCode = statusCode;
	}
}
