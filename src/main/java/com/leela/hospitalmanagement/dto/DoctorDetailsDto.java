package com.leela.hospitalmanagement.dto;

import lombok.Data;

@Data
public class DoctorDetailsDto {

	private Integer doctorId;
	
	private String doctorName;
	
	private String contactNumber;
	
	private String address;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String pincode;
	
	private Integer departmentId;
	
	private Integer sepecializationId;
	
}
