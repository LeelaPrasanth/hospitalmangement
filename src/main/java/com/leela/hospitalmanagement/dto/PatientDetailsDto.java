package com.leela.hospitalmanagement.dto;

import lombok.Data;

@Data
public class PatientDetailsDto {

	private Integer patientId;
	
	private String patientName;
	
	private String email;
	
	private String dateOfBirth;
	
	private String contactNumber;
	
	private String address;
	
	private String city;
	
	private String state;
	
	private String country;
	
	private String pincode;
	
	private String remarks;
	
}
