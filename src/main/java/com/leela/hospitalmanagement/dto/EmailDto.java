package com.leela.hospitalmanagement.dto;

import lombok.Data;

@Data
public class EmailDto {


	private String subject;
	private String body;
	private String to;
	private String from;
	
}
