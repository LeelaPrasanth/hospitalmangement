package com.leela.hospitalmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.leela.hospitalmanagement.entity.DoctorDetails;

@Repository
public interface DoctorDetailsRepository extends JpaRepository<DoctorDetails, Integer>{

	DoctorDetails findDetailsByDoctorName(String name);
	
}
