package com.leela.hospitalmanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.leela.hospitalmanagement.entity.TestDetails;

public interface TestDetailsRepository extends JpaRepository<TestDetails, Integer>{

}
