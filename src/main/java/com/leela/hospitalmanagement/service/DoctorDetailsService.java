package com.leela.hospitalmanagement.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.leela.hospitalmanagement.dto.DoctorDetailsDto;
import com.leela.hospitalmanagement.dto.ResponseStructure;
import com.leela.hospitalmanagement.entity.DoctorDetails;
import com.leela.hospitalmanagement.repository.DoctorDetailsRepository;

@Service
public class DoctorDetailsService {

	@Autowired
	private DoctorDetailsRepository doctorDetailsRepository;
	
	@Autowired
	private ModelMapper modelMapper;
	
	private static final Logger LOGGER=LoggerFactory.getLogger(DoctorDetails.class);
	
	ResponseStructure<DoctorDetails> response= new ResponseStructure<>();
	
	public ResponseStructure<DoctorDetails> saveDoctorDetails(DoctorDetailsDto dto)
	{
		LOGGER.info("inside save doctor details() of savedoctordetails doctordeatils {} :"+dto);
		DoctorDetails map = modelMapper.map(dto, DoctorDetails.class);
		response.setData(doctorDetailsRepository.save(map));response.setStatusCode(HttpStatus.FOUND.value());response.setMessage("saving DoctorDetails");
//		doctorDetailsRepository.save(map);
		return response;
	}
	
	
	public List<DoctorDetails> getAllDetails()
	{
		LOGGER.info("inside get all details() of getalldetails doctordeatils {} :");
		return doctorDetailsRepository.findAll();
	}
	
	public DoctorDetails getDetailsByDoctorName(String name)
	{
		LOGGER.info("inside get details by doctor name() of getdetailsbydoctorname doctordeatils {} :"+name);
		return doctorDetailsRepository.findDetailsByDoctorName(name);
	}
	
	public ResponseStructure<DoctorDetails> getDetailsByDoctorId(Integer id)
	{
		LOGGER.info("inside get details by doctor id() of getdetailsbydoctorid doctordeatils {} :"+id);
		Optional<DoctorDetails> findById = doctorDetailsRepository.findById(id);
		if(findById.isPresent())
		{
      		response.setData(findById.get());response.setStatusCode(HttpStatus.FOUND.value());response.setMessage("getting order details by id");
      		return response;
		}
		response.setData(null);response.setStatusCode(HttpStatus.FOUND.value());response.setMessage("not found details");
  		return response;
	}
	
}
