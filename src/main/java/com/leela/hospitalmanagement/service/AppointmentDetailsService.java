package com.leela.hospitalmanagement.service;

import java.sql.Date;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leela.hospitalmanagement.controller.DoctorScheduleController;
import com.leela.hospitalmanagement.dto.AppointmentDetailsDto;
import com.leela.hospitalmanagement.dto.EmailDto;
import com.leela.hospitalmanagement.entity.AppointmentDetails;
import com.leela.hospitalmanagement.entity.DoctorSchedule;
import com.leela.hospitalmanagement.repository.AppointmentDetailsRepository;
import com.leela.hospitalmanagement.repository.DoctorScheduleRepository;

/**
 * @author ASUS
 *	AppointmentDetailsService class is used to send details to AppointmentDetailsRepository 
 *	and save into database
 */
@Service
public class AppointmentDetailsService {

	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private DoctorScheduleController controller;
	
	@Autowired
	private DoctorScheduleRepository doctorScheduleRepository;
	
	@Autowired
	private AppointmentDetailsRepository appointmentDetailsRepository;
	
	@Autowired
	private RestUtilService restUtilService;
	
	/**
	 * @param dto
	 * Method to save appointment_details into database
	 */
	public void saveAppointmentDetails(AppointmentDetailsDto dto)
	{
		AppointmentDetails appointment= new AppointmentDetails();
		appointment.setPatientId(dto.getPatientId());
		appointment.setDoctorId(dto.getDoctorId());
		appointment.setDate(dto.getDate());
		appointment.setTime(dto.getTime());
		appointment.setIsConfirmed(verifyDoctorSchedule(dto.getDate(), dto.getTime(), dto.getDoctorId()));
		appointment.setPrescription(dto.getPrescription());
		appointmentDetailsRepository.save(appointment);
		
//		sending email to the required email id after 
		
	
	}
	
	
	/**
	 * @param date
	 * @param time
	 * @param doctorId
	 * @return
	 * this method is used get DoctorSchedule by using doctorID
	 * and compare patient appointment timing and DoctorSchedule timing if matched 
	 * updates the DoctorSchedule availableSlots and returns booked status
	 */
	public String verifyDoctorSchedule(Date date, String time, Integer doctorId) 
	{
		DoctorSchedule doctorScheduleById = controller.getDoctorScheduleByDoctorId(doctorId);
		System.out.println(doctorScheduleById);
		String scheduleTimings = doctorScheduleById.getScheduleTimings();
		if(verifyTimings(scheduleTimings, time))
		{
			if(doctorScheduleById.getPresentAvaialbeSlots()>0)
			{
				updateDoctorSchedule(doctorScheduleById);
				if(doctorScheduleById.getTotalAvailableSlots()==doctorScheduleById.getPresentAvaialbeSlots())
				{
					System.out.println(sendSucessEmailToPatient());
					sendSucessEmailToPatient();
					return "slot booked, your slot number is: 1";
				}
				sendSucessEmailToPatient();
				return "slot booked, your slot number is: "+(doctorScheduleById.getTotalAvailableSlots()-doctorScheduleById.getPresentAvaialbeSlots());
			}
			sendFailedEmailToPatient();
			return "all slots for the day are booked";
		}
		sendFailedEmailToPatient();
		return "you are trying to opt different time from our schedule, sorry please check the slots and retry: "+doctorScheduleById.getScheduleTimings(); 
	}	
	
	
	/**
	 * @param time
	 * @param patientTime
	 * @return
	 * this method used to convert String to String array by splitting " ,- "(comma,hyphen)symbols
	 * and matched patient time and doctor schedule and returned boolean value
	 */
	public boolean verifyTimings(String time,String patientTime)
	{
		String[] splitedTime = time.split("[,-]");
		if(splitedTime[0].equals(patientTime) || splitedTime[2].equals(patientTime))
		{
			return true;
		}
		return false;
	}
	
	
	/**
	 * @param doctorSchedule
	 * In this method we are updating the doctor_schedule table presentAvailableSlots value
	 */
	public void updateDoctorSchedule(DoctorSchedule doctorSchedule)
	{
		doctorSchedule.setScheduleId(doctorSchedule.getScheduleId());
		doctorSchedule.setDoctorId(doctorSchedule.getDoctorId());
		doctorSchedule.setTotalAvailableSlots(doctorSchedule.getTotalAvailableSlots());
		doctorSchedule.setPresentAvaialbeSlots(doctorSchedule.getPresentAvaialbeSlots()-1);
		doctorSchedule.setScheduleTimings(doctorSchedule.getScheduleTimings());
		doctorScheduleRepository.save(doctorSchedule);
	}
	
	
	public String sendSucessEmailToPatient() 
	{
		EmailDto emailDto=new EmailDto();
		emailDto.setFrom("leelaprasanth27@gmail.com");
		emailDto.setTo("bandelanagamaddiletty@gmail.com");
		emailDto.setBody("you have booked appointment successfully, thankyou \n visit hospital at the time scheduled for appointment.");
		emailDto.setSubject("Response to the doctor appointment");
		String post = restUtilService.post("https://localhost:8082/email/processEmail", emailDto);
		return post;
	}
	
	public String sendFailedEmailToPatient()
	{
		EmailDto emailDto=new EmailDto();
		emailDto.setFrom("leelaprasanth27@gmail.com");
		emailDto.setTo("bandelanagamaddiletty@gmail.com");
		emailDto.setBody("Your appointment has not been booked due to to some issue, sorry for inconvenience");
		emailDto.setSubject("Response to the doctor appointment");
		String post = restUtilService.post("https://localhost:8082/email/processEmail", emailDto);
		return post;
	}
	
}
