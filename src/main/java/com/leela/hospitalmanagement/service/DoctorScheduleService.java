package com.leela.hospitalmanagement.service;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leela.hospitalmanagement.dto.DoctorScheduleDto;
import com.leela.hospitalmanagement.entity.DoctorSchedule;
import com.leela.hospitalmanagement.repository.DoctorScheduleRepository;

@Service
public class DoctorScheduleService {

	@Autowired
	private DoctorScheduleRepository doctorScheduleRepository;
	
	@Autowired
	private ModelMapper mapper;
	
	public void saveDoctorSchedule(DoctorScheduleDto dto)
	{
		DoctorSchedule map = mapper.map(dto, DoctorSchedule.class);
		doctorScheduleRepository.save(map);
	}
	
	public DoctorSchedule getDoctorScheduleById(Integer id)
	{
		Optional<DoctorSchedule> findById = doctorScheduleRepository.findById(id);
		return findById.get();
	}
	
	public DoctorSchedule getDoctorScheduleByDoctorId(Integer doctorId)
	{
		return doctorScheduleRepository.findByDoctorId(doctorId);
	}

}
