package com.leela.hospitalmanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestUtilService {

	@Autowired
	private RestTemplate restTemplate;
	
	public String post(String url, Object data)
	{
		String object = restTemplate.postForObject(url, data, String.class);
		return object;
	}
}
