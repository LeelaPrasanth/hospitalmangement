package com.leela.hospitalmanagement.service;

import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leela.hospitalmanagement.dto.PatientDetailsDto;
import com.leela.hospitalmanagement.entity.PatientDetails;
import com.leela.hospitalmanagement.repository.PatientDetailsRepository;

@Service
public class PatientDetailsService {

	@Autowired
	private ModelMapper modelMapper;
	
	@Autowired
	private PatientDetailsRepository patientDetailsRepository;
	
	public void savePatientDetails(PatientDetailsDto dto)
	{
		PatientDetails map = modelMapper.map(dto, PatientDetails.class);
		patientDetailsRepository.save(map);
	}
	
	public List<PatientDetails> getAllDetails()
	{
		return patientDetailsRepository.findAll();
	}
	
	
	public Optional<PatientDetails> getPatientDetailsById(Integer id)
	{
		return patientDetailsRepository.findById(id);
	}
}
