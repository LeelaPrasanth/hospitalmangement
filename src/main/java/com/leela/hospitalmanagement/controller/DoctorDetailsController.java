package com.leela.hospitalmanagement.controller;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leela.hospitalmanagement.constants.HospitalManagementConstants;
import com.leela.hospitalmanagement.dto.DoctorDetailsDto;
import com.leela.hospitalmanagement.dto.ResponseStructure;
import com.leela.hospitalmanagement.entity.DoctorDetails;
import com.leela.hospitalmanagement.service.DoctorDetailsService;

@RestController
@RequestMapping(value=HospitalManagementConstants.FORWARD)
public class DoctorDetailsController {

	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private DoctorDetailsService detailsService;
	
	
	private static final Logger LOGGER=LoggerFactory.getLogger(DoctorDetails.class);

	
	@PostMapping(value=HospitalManagementConstants.SAVE_DOCTOR_DETAILS)
	public void saveDoctorDetails(@RequestBody DoctorDetailsDto dto)
	{
		try {
			LOGGER.info("inside save doctor details() of doctordetailscontroller doctordeatils {} :"+dto);
			detailsService.saveDoctorDetails(dto);
		}
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@ResponseBody
	@GetMapping(value=HospitalManagementConstants.GET_ALL_DETAILS)
	public List<DoctorDetails> getAllDetails()
	{
		LOGGER.info("inside get all doctor details() of getalldoctordetails doctordeatils {} :");
		try {
			
			List<DoctorDetails> allDetails = detailsService.getAllDetails();
			return allDetails;
		} 
		catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	@ResponseBody
	@GetMapping(value=HospitalManagementConstants.GET_DETAILS_BY_DOCTOR_NAME)
	public DoctorDetails getDetailsByDoctorName(String name)
	{
		try {
			LOGGER.info("inside get details by doctor name() of getdetailsbydoctorname doctordeatils {} :"+name);
			return detailsService.getDetailsByDoctorName(name);
		} 
		catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	
	@ResponseBody
	@GetMapping(value=HospitalManagementConstants.GET_DETAILS_BY_DOCTOR_ID)
	public ResponseStructure<DoctorDetails> getDetailsByDoctorId(Integer id)
	{
		try {
			LOGGER.info("inside get details by doctor id() of getdetailsbydoctorid doctordeatils {} :"+id);
			return detailsService.getDetailsByDoctorId(id);
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
}
