package com.leela.hospitalmanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.leela.hospitalmanagement.constants.HospitalManagementConstants;
import com.leela.hospitalmanagement.dto.AppointmentDetailsDto;
import com.leela.hospitalmanagement.service.AppointmentDetailsService;

@RestController
@RequestMapping(value=HospitalManagementConstants.FORWARD)
public class AppointmentDetailsController {

	@Autowired
	private AppointmentDetailsService appointmentDetailsService;
	
	@PostMapping(value=HospitalManagementConstants.BOOK_APPOINTMENT)
	public void bookAppointment(@RequestBody AppointmentDetailsDto dto)
	{
		appointmentDetailsService.saveAppointmentDetails(dto);
	}
}
