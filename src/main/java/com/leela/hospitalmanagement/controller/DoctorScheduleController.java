package com.leela.hospitalmanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leela.hospitalmanagement.constants.HospitalManagementConstants;
import com.leela.hospitalmanagement.dto.DoctorScheduleDto;
import com.leela.hospitalmanagement.entity.DoctorSchedule;
import com.leela.hospitalmanagement.service.DoctorScheduleService;

@RestController
@RequestMapping(value=HospitalManagementConstants.FORWARD)
public class DoctorScheduleController {

	@Autowired
	private DoctorScheduleService doctorScheduleService;
	
	@PostMapping(value =HospitalManagementConstants.SAVE_DOCTOR_SCHEDULE)
	public void saveDoctorSchedule(@RequestBody DoctorScheduleDto dto)
	{
		doctorScheduleService.saveDoctorSchedule(dto);
	}
	
	@ResponseBody
	@GetMapping(value=HospitalManagementConstants.GET_DOCTOR_SCHEDULE_BY_ID)
	public DoctorSchedule getDoctorScheduleById(Integer id)
	{
		return doctorScheduleService.getDoctorScheduleById(id);
	}
	
	@ResponseBody
	@GetMapping(value=HospitalManagementConstants.GET_DOCTOR_SCHEDULE_BY_DOCTOR_ID)
	public DoctorSchedule getDoctorScheduleByDoctorId(Integer doctorId)
	{
		return doctorScheduleService.getDoctorScheduleByDoctorId(doctorId);
	}
	
}
