package com.leela.hospitalmanagement.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.leela.hospitalmanagement.constants.HospitalManagementConstants;
import com.leela.hospitalmanagement.dto.PatientDetailsDto;
import com.leela.hospitalmanagement.entity.PatientDetails;
import com.leela.hospitalmanagement.service.PatientDetailsService;

@RestController
@RequestMapping(value=HospitalManagementConstants.FORWARD)
public class PatientDetailsController {

	@Autowired
	private PatientDetailsService detailsService;
	
	@PostMapping(value=HospitalManagementConstants.SAVE_PATIENT_DETAILS)
	public void savePatientDetails(@RequestBody PatientDetailsDto dto)
	{
		detailsService.savePatientDetails(dto);
	}
	
	@ResponseBody
	@GetMapping(value=HospitalManagementConstants.GET_ALL_PATIENT_DETAILS)
	public List<PatientDetails> getAllPatientDetails()
	{
		return detailsService.getAllDetails();
	}
	
	@ResponseBody
	@GetMapping(value=HospitalManagementConstants.GET_PATIENT_DETAILS_BY_ID)
	public Optional<PatientDetails> getPatientDetailsById(Integer id)
	{
		return detailsService.getPatientDetailsById(id);
	}
}
