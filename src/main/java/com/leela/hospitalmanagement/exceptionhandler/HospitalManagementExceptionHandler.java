package com.leela.hospitalmanagement.exceptionhandler;

public class HospitalManagementExceptionHandler extends RuntimeException {

	public String message;
	
	
	public HospitalManagementExceptionHandler(String message) {
		this.message = message;
	}


	public String getExcMessage() {
		return message;
	}
	
	
}
