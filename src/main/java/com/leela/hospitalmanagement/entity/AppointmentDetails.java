package com.leela.hospitalmanagement.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.leela.hospitalmanagement.constants.HospitalManagementConstants;

import lombok.Data;

@Data
@Entity
@Table(name=HospitalManagementConstants.APPOINTMENT_DETAILS)
public class AppointmentDetails implements Serializable {

	@Id
	@GenericGenerator(name="auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name="appointment_id")
	private Integer appointmentId;
	
	@Column(name="patient_id")
	private Integer patientId;
	
	@Column(name="doctor_id")
	private Integer doctorId;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="time")
	private String time;
	
	@Column(name="is_confirmed")
	private String isConfirmed;
	
	@Column(name="prescription")
	private String prescription;
	
}
