package com.leela.hospitalmanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.leela.hospitalmanagement.constants.HospitalManagementConstants;

import lombok.Data;

/**
 * @author Prasanth
 * DoctorDetails entity class to connect column and values to database using Serializable
 * we created private datamembers
 * implemented getters and setters
 * overridden toString
 */
@Data
@Entity
@Table(name=HospitalManagementConstants.DOCTOR_DETAILS)
public class DoctorDetails implements Serializable{
	
	@Id
	@Column(name="doctor_id")
	private Integer doctorId;
	
	@Column(name="name")
	private String doctorName;
	
	@Column(name="contact_number")
	private String contactNumber;
	
	@Column(name="address")
	private String address;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="country")
	private String country;
	
	@Column(name="pincode")
	private String pincode;
	
	@Column(name="dept_id")
	private Integer departmentId;
	
	@Column(name="spec_id")
	private Integer sepecializationId;
	
		
}
