package com.leela.hospitalmanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.leela.hospitalmanagement.constants.HospitalManagementConstants;

import lombok.Data;

@Data
@Entity
@Table(name=HospitalManagementConstants.DOCTOR_SCHEDULE)
public class DoctorSchedule implements Serializable {

	@Id
	@GenericGenerator(name="auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name="schedule_id")
	private Integer scheduleId;
	
	@Column(name="doctor_id")
	private Integer doctorId;
	
	@Column(name="tital_available_slots")
	private Integer totalAvailableSlots;
	
	@Column(name="present_available_slots")
	private Integer presentAvaialbeSlots;
	
	@Column(name="schedule_timings")
	private String scheduleTimings;
	
}
