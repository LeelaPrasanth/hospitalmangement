package com.leela.hospitalmanagement.entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.leela.hospitalmanagement.constants.HospitalManagementConstants;

import lombok.Data;

@Data
@Entity
@Table(name=HospitalManagementConstants.TEST_DETAILS)
public class TestDetails implements Serializable{

	@Id
	@GenericGenerator(name="auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name="test_id")
	private Integer testId;
	
	@Column(name="patient_id")
	private Integer patientId;
	
	@Column(name="appointment_id")
	private Integer appointmentId;
	
	@Column(name="name")
	private String name;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="doctor_id")
	private Integer doctorId;
	
	@Column(name="remarks")
	private String remarks;
	
	
}
