package com.leela.hospitalmanagement.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.leela.hospitalmanagement.constants.HospitalManagementConstants;

import lombok.Data;

@Data
@Entity
@Table(name=HospitalManagementConstants.PATIENT_DETAILS)
public class PatientDetails implements Serializable{

	@Id
	@GenericGenerator(name="auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name="patient_id")
	private Integer patientId;
	
	@Column(name="name")
	private String patientName;
	
	@Column(name="email")
	private String email;
	
	@Column(name="date_of_birth")
	private String dateOfBirth;
	
	@Column(name="contact_number")
	private String contactNumber;
	
	@Column(name="address")
	private String address;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="country")
	private String country;
	
	@Column(name="pincode")
	private String pincode;
	
	@Column(name="remarks")
	private String remarks;
	
	
}
