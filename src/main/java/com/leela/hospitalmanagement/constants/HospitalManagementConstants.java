package com.leela.hospitalmanagement.constants;

/**
 * @author Prasanth
 * Constants interface for adding constants
 */
public interface HospitalManagementConstants {

	public static String DOCTOR_DETAILS="doctor_details";
	public static String PATIENT_DETAILS="patient_details";
	public static String APPOINTMENT_DETAILS="appointment_details";
	public static String DOCTOR_SCHEDULE="doctor_schedule";
	
	
	
	public static String FORWARD="/";
	
	public static String SAVE_DOCTOR_DETAILS="/saveDoctorDetails";
	public static String GET_ALL_DETAILS="/getAllDetails";
	public static String GET_DETAILS_BY_DOCTOR_NAME="/getDetailsByDoctorName";
	public static String GET_DETAILS_BY_DOCTOR_ID="/getDetailsByDoctorId";
	
	
	public static String GET_PATIENT_DETAILS_BY_ID="/getPatientDetailsById";
	public static String GET_ALL_PATIENT_DETAILS="/getAllPatientDetails";
	public static String SAVE_PATIENT_DETAILS="/savePatientDetails";
	
	public static String TEST_DETAILS ="/test_details";
	public static String BOOK_APPOINTMENT="/bookAppointment";
	
	public static String GET_DOCTOR_SCHEDULE_BY_ID="/getDoctorScheduleById";
	public static String SAVE_DOCTOR_SCHEDULE="/saveDoctorSchedule";
	public static String GET_DOCTOR_SCHEDULE_BY_DOCTOR_ID="/getDoctorScheduleByDoctorId";
}
